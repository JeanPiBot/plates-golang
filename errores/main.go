package main

import (
	"fmt"
	"log"
	"strconv"
)

func isError (e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func main() {
	_, err := strconv.Atoi("53a")
	isError(err)

	// Map
	m:= make(map[string]int)
	m["Jean Pierre"] = 1

	value, ok := m["Jean Pierre"]

	fmt.Println(value, ok)
}